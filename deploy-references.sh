#!/bin/sh

# Push all png 

set -e

git config --global push.default simple
git config user.name "Gitlab CI"
git config user.email "${GITLAB_USER_EMAIL}"

git lfs install
git lfs track "*.png"
git add .gitattributes

git checkout "${CI_COMMIT_REF_NAME}"
git add --all
git diff-index --quiet HEAD || git commit -m "Update reference png for commits: ${CI_COMMIT_BEFORE_SHA}" -m "Gitlab CI build [ci skip]"
git push "${CI_REPOSITORY_URL}" "${CI_COMMIT_REF_NAME}"
